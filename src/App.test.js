import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

describe('attributes', () => {
  it ('uses the right homepage', () => {
    const app = new App()
    expect(app.HomePage).toEqual('order-food-app.netlify.com')
  })

  it ('gets the right color scheme for the env.', () => {
    const app = new App()
    expect(app.colorScheme()).toEqual('blue')
  })
})

