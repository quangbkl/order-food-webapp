import React from 'react';
import {Button, Card, CardBody, Col, Form, FormFeedback, FormGroup, Input, Label, Row} from "reactstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSpinner} from "@fortawesome/free-solid-svg-icons";
import ItemDetails from "./ItemDetails";
import apiOrders from '../../../services/orders/index';
import {Redirect} from "react-router-dom";

class CheckoutDetails extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            firstName: '',
            lastName: '',
            email: '',
            type: 'shipping',
            company: '',
            phoneNumber: '',
            address: '',
            address2: '',
            city: '',
            country: '',
            province: '',
            zipCode: '',
            isLoadingButton: false,
            orderId: ''
        }
    }

    handleChangeInput = (e) => {
        const {name, value} = e.target;
        this.setState({
            [name]: value,
            [name + 'Invalid']: false
        });
    }

    validateEmail = (email) => {
        const re = new RegExp(`^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$`);
        return (re.test(email));
    }

    handleValidateForm = () => {
        const {firstName, lastName, email, address, country, province} = this.state;

        const objectInvalid = {};
        if (!firstName) objectInvalid.firstNameInvalid = true;
        if (!lastName) objectInvalid.lastNameInvalid = true;
        if (!address) objectInvalid.addressInvalid = true;
        if (!country) objectInvalid.countryInvalid = true;
        if (!province) objectInvalid.provinceInvalid = true;

        const invalidEmail = !this.validateEmail(email) && !!email;
        if (invalidEmail) objectInvalid.emailInvalid = true;

        return new Promise(resolve => {
            this.setState(objectInvalid, () => {
                const result = !!firstName && !!lastName && !!address && !!country && !!province && !invalidEmail;
                resolve(result);
            });
        });
    }

    handleOnSubmit = async () => {
        const isValid = await this.handleValidateForm();
        if (!isValid) return {success: false, message: 'Form invalid.'};

        try {
            const {firstName, lastName, email, type, company, phoneNumber, address, address2, city, country, province, zipCode} = this.state;
            const quantity = this.props.quantity;
            const price = this.props.product.price;

            const order = {
                product: this.props.product._id,
                quantity: this.props.quantity,
                type,
                pricing_summary: {
                    subtotal: price * quantity,
                    total: price * quantity
                },
                ship_to: {
                    first_name: firstName,
                    last_name: lastName,
                    email,
                    company,
                    phone_number: phoneNumber,
                    contact_address: {
                        address,
                        address2,
                        city,
                        province,
                        zip_code: zipCode,
                        country
                    }
                }
            }

            this.setState({
                isLoadingButton: true
            });
            const resCreate = await apiOrders.create(order);
            this.setState({
                isLoadingButton: false
            });

            if (!resCreate.data.success && resCreate.data.hasOwnProperty('message')) throw new Error(resCreate.data.message);
            const {_id} = resCreate.data.data;

            this.setState({
                orderId: _id
            });

            return resCreate.data;
        } catch (e) {
            this.setState({
                isLoadingButton: false
            });
            this.props.handleError(e.message);
        }
    }

    render() {
        if (!!this.state.orderId) return <Redirect to={'/invoice/' + this.state.orderId}/>;

        const loadingIcon = this.state.isLoadingButton ? <FontAwesomeIcon className='spinner' icon={faSpinner}/> : '';

        return (
            <div className='CheckoutDetails'>
                <Card>
                    <CardBody>
                        <ItemDetails product={this.props.product} quantity={this.props.quantity}/>
                    </CardBody>
                </Card>
                <h4>Customer Information</h4>
                <Form>
                    <Row form>
                        <Col md={6}>
                            <FormGroup>
                                <Label for="FirstName">First name</Label>
                                <Input type="text" name="firstName" id="FirstName"
                                       placeholder="Enter the first name..." onChange={this.handleChangeInput}
                                       value={this.state.firstName} invalid={this.state.firstNameInvalid}/>
                                <FormFeedback>Oh noes! First name is not null.</FormFeedback>
                            </FormGroup>
                        </Col>
                        <Col md={6}>
                            <FormGroup>
                                <Label for="LastName">Last name</Label>
                                <Input type="text" name="lastName" id="LastName"
                                       placeholder="Enter the last name..." onChange={this.handleChangeInput}
                                       value={this.state.lastName} invalid={this.state.lastNameInvalid}/>
                                <FormFeedback>Oh noes! Last name is not null.</FormFeedback>
                            </FormGroup>
                        </Col>
                    </Row>
                    <FormGroup>
                        <Label for="Email">Email</Label>
                        <Input type="email" name="email" id="Email" placeholder="Enter the email..."
                               onChange={this.handleChangeInput} value={this.state.email}
                               invalid={this.state.emailInvalid}/>
                        <FormFeedback>Oh noes! You have entered an invalid email address.</FormFeedback>
                    </FormGroup>
                    <FormGroup>
                        <Label for="SelectType">Type</Label>
                        <Input type="select" name="type" id="SelectType" onChange={this.handleChangeInput}
                               value={this.state.type}>
                            <option value='shipping'>Shipping</option>
                            <option value='reservations'>Reservations</option>
                        </Input>
                    </FormGroup>
                    <Row form>
                        <Col md={6}>
                            <FormGroup>
                                <Label for="Company">Company</Label>
                                <Input type="text" name="company" id="Company"
                                       placeholder="Enter the company..." onChange={this.handleChangeInput}
                                       value={this.state.company}/>
                            </FormGroup>
                        </Col>
                        <Col md={6}>
                            <FormGroup>
                                <Label for="PhoneNumber">Phone number</Label>
                                <Input type="text" name="phoneNumber" id="PhoneNumber"
                                       placeholder="Enter the phone number..." onChange={this.handleChangeInput}
                                       value={this.state.phoneNumber}/>
                            </FormGroup>
                        </Col>
                    </Row>
                    <h4>Shipping Address</h4>
                    <Row form>
                        <Col md={6}>
                            <FormGroup>
                                <Label for="Address">Address</Label>
                                <Input type="text" name="address" id="Address"
                                       placeholder="Enter the address..." onChange={this.handleChangeInput}
                                       value={this.state.address} invalid={this.state.addressInvalid}/>
                                <FormFeedback>Oh noes! Address is not null.</FormFeedback>
                            </FormGroup>
                        </Col>
                        <Col md={6}>
                            <FormGroup>
                                <Label for="Address2">Apartment, suite, etc. (optional)</Label>
                                <Input type="text" name="address2" id="Address2"
                                       placeholder="Enter the apartment, suite, etc."
                                       onChange={this.handleChangeInput} value={this.state.address2}/>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row form>
                        <Col md={6}>
                            <FormGroup>
                                <Label for="City">City</Label>
                                <Input type="text" name="city" id="City"
                                       placeholder="Enter the city..." onChange={this.handleChangeInput}
                                       value={this.state.city}/>
                            </FormGroup>
                        </Col>
                        <Col md={6}>
                            <FormGroup>
                                <Label for="Country">Country</Label>
                                <Input type="text" name="country" id="Country"
                                       placeholder="Enter the country..." onChange={this.handleChangeInput}
                                       value={this.state.country} invalid={this.state.countryInvalid}/>
                                <FormFeedback>Oh noes! Country is not null.</FormFeedback>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row form>
                        <Col md={6}>
                            <FormGroup>
                                <Label for="Province">Province or State</Label>
                                <Input type="text" name="province" id="Province"
                                       placeholder="Enter the province..." onChange={this.handleChangeInput}
                                       value={this.state.province} invalid={this.state.provinceInvalid}/>
                                <FormFeedback>Oh noes! Province or State is not null.</FormFeedback>
                            </FormGroup>
                        </Col>
                        <Col md={6}>
                            <FormGroup>
                                <Label for="ZipCode">ZIP/Postal code</Label>
                                <Input type="text" name="zipCode" id="ZipCode"
                                       placeholder="Enter the zip code..." onChange={this.handleChangeInput}
                                       value={this.state.zipCode}/>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Button className='btn-ladda btn-ladda-spinner ladda-button'
                            color='primary' size='lg' block
                            disabled={this.state.isLoadingButton}
                            onClick={this.handleOnSubmit}>
                        Order
                        {loadingIcon}
                    </Button>
                </Form>
            </div>
        );
    }
}

export default CheckoutDetails;