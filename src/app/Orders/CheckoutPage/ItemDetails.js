import React from 'react';
import Row from "reactstrap/es/Row";
import {Col, Table} from "reactstrap";

class ItemDetails extends React.Component {
    render() {
        const quantity = this.props.quantity;
        const price = this.props.product.price.toFixed(2);
        const {pictures} = this.props.product;
        const thumbnail = Array.isArray(pictures) ? pictures[0] : null;

        return (
            <div className='ItemDetails'>
                <Row>
                    <Col md={6} className='OrderListItems'>
                        <Table responsive>
                            <tbody>
                            <tr>
                                <td>
                                    <img
                                        src={thumbnail}
                                        alt='thumbnail'/>
                                </td>
                                <td>{this.props.product.title}</td>
                                <td>${price + ' × ' + quantity}</td>
                            </tr>
                            </tbody>
                        </Table>
                    </Col>
                    <Col md={6} className='OrderDetails'>
                        <div className="OrderDetailsInner">
                            <div className="Top">
                                <div className="Subtotal d-flex justify-content-between">
                                    <div className="Name">Subtotal:</div>
                                    <div className="Amount">${price * quantity}</div>
                                </div>
                                <div className="Shipping d-flex justify-content-between">
                                    <div className="Name">Shipping:</div>
                                    <div className="Amount">$0.00</div>
                                </div>
                                <div className="Total d-flex justify-content-between">
                                    <div className="Name">Total:</div>
                                    <div className="Amount">${price * quantity}</div>
                                </div>
                            </div>
                            <div className="Bottom">
                                <div className="Paid d-flex justify-content-between align-items-center">
                                    <div className="Name">Paid:</div>
                                    <div className="Amount">${price * quantity}</div>
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default ItemDetails;