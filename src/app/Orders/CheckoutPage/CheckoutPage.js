import React from 'react';
import queryString from 'query-string';
import {Alert, Card, CardBody} from "reactstrap";
import apiProducts from '../../../services/products/index';
import Loading from "../../../components/Loading/Loading";
import CheckoutDetails from "./CheckoutDetails";

class CheckoutPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            product: null,
            isLoading: true,
            quantity: 1,
            error: ''
        }
    }

    componentDidMount() {
        this.loadStateFromApi()
            .then(res => {
                this.loadStateFromParams();
            });
    }

    loadStateFromApi = async () => {
        try {
            const productId = this.props.match.params.productId;
            const resProduct = await apiProducts.read(productId);
            if (!resProduct.data.success) throw new Error(resProduct.data.message);
            this.setState({
                product: resProduct.data.data,
                error: '',
                isLoading: false
            });
        } catch (e) {
            this.setState({
                error: e.message,
                isLoading: false
            });
        }
    }

    loadStateFromParams = () => {
        const parsedParams = this.parseParams();
        const quantity = parsedParams.quantity || 1;

        this.setState({quantity: parseInt(quantity)});
    }

    parseParams = () => {
        const {location} = this.props;
        const parsed = queryString.parse(location.search);

        return parsed;
    }

    handleError = (message) => {
        this.setState({
            error: message
        });
    }

    render() {
        if (this.state.isLoading) return <Loading/>;
        const errorComponent = !!this.state.error ? <Alert color='danger'>{this.state.error}</Alert> : '';
        const checkoutDetailsComponent = !!this.state.product
            ? <CheckoutDetails product={this.state.product}
                               quantity={this.state.quantity}
                               handleError={this.handleError}/> : '';

        return (
            <Card className='CheckoutPage'>
                <CardBody>
                    {errorComponent}
                    {checkoutDetailsComponent}
                </CardBody>
            </Card>
        );
    }
}

export default CheckoutPage;