import React from 'react';
import CarouselHome from "./CarouselHome";
import {Button, Col, Row} from "reactstrap";
import {Link} from "react-router-dom";

class HomePage extends React.Component {
    render() {
        return (
            <div className='HomePage'>
                <Row className='RowSection'>
                    <Col md={6}>
                        <h2 className='Title'>Order Food</h2>
                        <h3 className='Subtitle'>Eat.Enjoy.Share</h3>
                    </Col>
                    <Col md={6}>
                        <img src='http://www.pngmart.com/files/8/Grilled-Food-PNG-Download-Image.png' alt=''/>
                </Col>
                </Row>

                <CarouselHome/>

                <Row className='RowSection'>
                    <Col md={6}>
                        <img src='http://pluspng.com/img-png/png-plate-of-food--1502.png' alt=''/>
                    </Col>
                    <Col md={6}>
                        <h2 className='Title'>Hungry?</h2>
                        <h3 className='Subtitle'>Order from Order Food</h3>
                    </Col>
                </Row>

                <div className='FooterHP'>
                    <h2 className='Title'>Order delicious food online!</h2>
                    <Link to={'/store'}>
                        <Button color='primary mt-4' size='lg'>Shop Now</Button>
                    </Link>
                </div>
            </div>
        );
    }
}

export default HomePage;