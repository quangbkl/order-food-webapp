import React from 'react';
import {UncontrolledCarousel} from 'reactstrap';

const items = [
    {
        src: 'http://www.pngall.com/wp-content/uploads/2016/03/Food-PNG.png'
    },
    {
        src: 'https://stickeroid.com/uploads/pic/full/fadc5e9098266ef7e6a36222e25056946d04d453.png'
    },
    {
        src: 'https://scontent.fhan3-2.fna.fbcdn.net/v/t1.0-9/57441698_2292398544184836_8973604750696120320_n.jpg?_nc_cat=107&_nc_oc=AQlsvlE_Z2b-1hdp0z1J4VQJWNApxbW7P_N2myy5e4m_crgWKIenh9ps9B-xqlZvI8Y&_nc_ht=scontent.fhan3-2.fna&oh=99655314d49e056537661032f2566ce5&oe=5D71D734'
    }
];

const CarouselHome = () => <UncontrolledCarousel items={items}/>;

export default CarouselHome;