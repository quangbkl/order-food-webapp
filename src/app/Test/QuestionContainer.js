import React from 'react'
import AnswerContainer from "./AnswerContainer";

class QuestionContainer extends React.Component {
    render() {
        const {question} = this.props
        const answers = question.answers.sort(() => 0.5 - Math.random())

        return (
            <div className='QuestionContainer'>
                <div className="form-group pt-2">
                    <label className="font-weight-semibold">{question.question}</label>
                    {
                        answers.map(answer => {
                            return <AnswerContainer key={answer.key} answer={answer}
                                                    correctAnswers={question.correctAnswers}/>
                        })
                    }
                </div>
            </div>
        );
    }
}

export default QuestionContainer
