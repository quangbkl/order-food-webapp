import React from 'react'
import QuestionContainer from "./QuestionContainer";
import test from '../../config/test'
import {Button} from "reactstrap";
import PubSub from 'pubsub-js'
import {Link} from "react-router-dom";

class TestAll extends React.Component {
    handleClickSubmit = () => {
        PubSub.publish('SUBMIT_ANSWER', 'submit answer');
    }

    makeAllQuestions = () => {
        const questions = []
        for (let value of Object.values(test)) {
            questions.push(...value)
        }
        return questions
    }

    render() {
        const {match: {params: {id}}} = this.props
        const questions = id === 'all' ? this.makeAllQuestions() : test[id].sort(() => 0.5 - Math.random())
        // const questions = id === 'all' ? this.makeAllQuestions() : test[id]

        return (
            <div className='TestAll'>
                <div>
                    <Link className='mr-2' to={'/test/all'}>all</Link>
                    {
                        Object.keys(test).map(key => {
                                return <Link key={key} className='mr-2' to={'/test/' + key}>{key}</Link>
                            }
                        )
                    }
                </div>
                {
                    questions.map((item, i) => {
                        return <QuestionContainer key={id + i} question={item}/>
                    })
                }
                <div className='text-right my-3'>
                    <Button color='primary' onClick={this.handleClickSubmit}>Submit</Button>
                </div>
            </div>
        );
    }
}

export default TestAll
