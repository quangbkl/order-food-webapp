import React from 'react';
import {UncontrolledCarousel} from "reactstrap";

class ProductCarousel extends React.Component {
    loadItemsFromProps = () => {
        const {pictures} = this.props;

        return pictures.map(picture => {
            return {
                src: picture
            };
        });
    }

    render() {
        const items = this.loadItemsFromProps();

        return (
            <UncontrolledCarousel items={items}/>
        );
    }
}

export default ProductCarousel;