import React from 'react';
import {Button, CardTitle, Col, FormGroup, Input, InputGroup, InputGroupAddon, Label, Row} from "reactstrap";
import {Link} from "react-router-dom";
import ProductCarousel from "./ProductCarousel";

class ProductDetails extends React.Component {
    render() {
        const checkoutLink = `/checkout/${this.props.product._id}?quantity=${this.props.quantity}`;

        return (
            <div className='ProductDetails'>
                <Row>
                    <Col md={6}>
                        <ProductCarousel pictures={this.props.product.pictures}/>
                    </Col>
                    <Col md={6}>
                        <CardTitle className='ProductTitle'>{this.props.product.title}</CardTitle>
                        <p className='ProductAddress'>{this.props.product.address}</p>
                        <h3 className='ProductPrice'>${this.props.product.price.toFixed(2)}</h3>
                        <FormGroup>
                            <Label for="ProductQuantity">
                                <b>Quantity</b>
                            </Label>
                            <InputGroup id='ProductQuantity'>
                                <InputGroupAddon addonType="prepend">
                                    <Button color='primary' onClick={this.props.handleChangeQuantity(-1)}>-</Button>
                                </InputGroupAddon>
                                <Input className='QuantityInput' placeholder="Enter the quantity..."
                                       type="number" name='quantity' value={this.props.quantity}
                                       min={1}
                                       onChange={this.props.handleChangeInput}/>
                                <InputGroupAddon addonType="append">
                                    <Button color='primary' onClick={this.props.handleChangeQuantity(1)}>+</Button>
                                </InputGroupAddon>
                            </InputGroup>
                        </FormGroup>
                        <Link to={checkoutLink}>
                            <Button color='primary' size='lg' block>Order Now</Button>
                        </Link>
                    </Col>
                </Row>
                <Row>
                    <div className='DescriptionTitle'>
                        DESCRIPTION
                    </div>
                    <div className='DescriptionContent'
                         dangerouslySetInnerHTML={{__html: this.props.product.description}}/>
                </Row>
            </div>
        )
    }
}

ProductDetails.defaultProps = {
    product: {}
}

export default ProductDetails;
