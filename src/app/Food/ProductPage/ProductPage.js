import React from 'react';
import {Alert, Card, CardBody} from "reactstrap";
import Loading from "../../../components/Loading/Loading";
import apiProducts from '../../../services/products/index';
import ProductDetails from "./ProductDetails";

class ProductPage extends React.Component {
    state = {
        product: {},
        error: '',
        isLoading: true,
        quantity: 1
    }

    componentDidMount() {
        this.loadStateFromApi();
    }

    loadStateFromApi = async () => {
        try {
            const productId = this.props.match.params.productId;
            const resProduct = await apiProducts.read(productId);
            if (!resProduct.data.success) throw new Error(resProduct.data.message);
            this.setState({
                product: resProduct.data.data,
                error: '',
                isLoading: false
            });
        } catch (e) {
            this.setState({
                error: e.message,
                isLoading: false
            });
        }
    }

    handleChangeInput = (e) => {
        const {name, value} = e.target;
        if (value <= 0) return;

        this.setState({
            [name]: parseInt(value, 10)
        });
    }

    handleChangeQuantity = (add) => () => {
        const {quantity} = this.state;
        const newValue = quantity + add;
        if (newValue > 0) {
            this.setState({
                quantity: newValue
            });
        }
    }

    render() {
        if (this.state.isLoading) return <Loading/>;

        const errorComponent = !!this.state.error ? <Alert color='danger'>{this.state.error}</Alert> : '';
        const productDetails = !this.state.error && !this.state.isLoading
            ? <ProductDetails product={this.state.product}
                              quantity={this.state.quantity}
                              handleChangeInput={this.handleChangeInput}
                              handleChangeQuantity={this.handleChangeQuantity}/> : '';

        return (
            <Card className='ProductPage'>
                <CardBody>
                    {errorComponent}
                    {productDetails}
                </CardBody>
            </Card>
        );
    }
}

export default ProductPage;