import React from 'react';
import apiProducts from "../../../services/products";
import {Alert, Button, Input, InputGroup, InputGroupAddon} from "reactstrap";
import Loading from "../../../components/Loading/Loading";
import ProductGridPage from "../ProductGrid/ProductGridPage";
import Pagination from "../../../components/Pagination/Pagination";
import {Redirect} from "react-router-dom";
import queryString from "query-string";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSearch} from "@fortawesome/free-solid-svg-icons";

class ProductsPage extends React.Component {
    state = {
        products: [],
        error: '',
        isLoading: true,
        pagination: {
            page: 1,
            limit: 24,
            total: 0
        },
        redirect: false
    }

    componentDidMount() {
        this.loadStateFromAll();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if ((prevState.pagination.page !== this.state.pagination.page || prevState.pagination.limit !== this.state.pagination.limit)) {
            this.setRedirect();
        }

        if (prevState.redirect !== this.state.redirect && !this.state.redirect) {
            this.loadStateFromAll();
        }
    }

    loadStateFromAll = () => {
        this.loadStateFromParams()
            .then(() => {
                this.loadStateFromApi();
            });
    }

    loadStateFromApi = async () => {
        try {
            const resProducts = await apiProducts.reads({
                page: this.state.pagination.page,
                limit: this.state.pagination.limit
            });
            if (!resProducts.data.success) throw new Error(resProducts.data.message);
            if (!resProducts.data.data) throw new Error('Response products invalid.');

            this.setState({
                products: resProducts.data.data,
                error: '',
                isLoading: false,
                pagination: Object.assign({}, this.state.pagination, {total: resProducts.data.total})
            });
        } catch (e) {
            this.setState({
                error: e.message,
                isLoading: false
            });
        }
    }

    loadStateFromParams = () => {
        const params = this.parseParams();
        const {page: pageStr, limit: limitStr} = params;
        const page = !!pageStr ? parseInt(pageStr) : pageStr;
        const limit = !!limitStr ? parseInt(limitStr) : limitStr;

        const newPagination = {
            page: !!page && page > 0 ? page : 1,
            limit: !!limit && limit > 0 ? limit : 24
        }

        return new Promise(resolve => {
            this.setState({
                pagination: Object.assign({}, this.state.pagination, newPagination)
            }, () => {
                resolve();
            });
        });
    }

    parseParams = () => {
        const {location} = this.props;
        const parsed = queryString.parse(location.search);

        return parsed;
    }

    setRedirect = () => {
        this.setState({
            redirect: true
        });
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            this.setState({
                redirect: false
            });

            return <Redirect to={`/store?page=${this.state.pagination.page}&limit=${this.state.pagination.limit}`}/>
        }
    }

    renderError = () => {
        if (this.state.error) {
            return <Alert color='danger'>{this.state.error}</Alert>;
        }
    }

    changePagination = (page) => {
        const newPagination = {page};

        this.setState({
            pagination: Object.assign({}, this.state.pagination, newPagination)
        });
    }

    render() {
        if (this.state.isLoading) return <Loading/>;
        const {pagination} = this.state;
        const totalPagination = pagination.total > 0 && pagination.limit > 0 ? Math.ceil(pagination.total / pagination.limit) : 1;

        return (
            <div className='ProductsPage'>
                {this.renderRedirect()}
                {this.renderError()}
                {/*<InputGroup>*/}
                    {/*<Input type='text'/>*/}
                    {/*<InputGroupAddon addonType="append">*/}
                        {/*<Button>*/}
                            {/*<FontAwesomeIcon icon={faSearch}/>*/}
                        {/*</Button>*/}
                    {/*</InputGroupAddon>*/}
                {/*</InputGroup>*/}
                <ProductGridPage products={this.state.products}/>
                <div className='PaginationContainer'>
                    <Pagination page={pagination.page} total={totalPagination} onChange={this.changePagination}/>
                </div>
            </div>
        );
    }
}

export default ProductsPage;