import React, {Component} from 'react';
import {Col, Row} from "reactstrap";
import ProductGridCard from "./ProductGridCard";

class ProductGridPage extends Component {
    render() {
        return (
            <div className='ProductGridPage'>
                <Row>
                    {
                        this.props.products.map((product, index) => {
                            return <Col key={index} xs={12} sm={12} md={6} lg={4} xl={3}>
                                <ProductGridCard product={product}/>
                            </Col>;
                        })
                    }
                </Row>
            </div>
        );
    }
}

export default ProductGridPage;