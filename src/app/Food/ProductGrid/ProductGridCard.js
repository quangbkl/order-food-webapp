import React from 'react';
import {Button, Card, CardBody} from "reactstrap";
import {Link} from "react-router-dom";

class ProductGridCard extends React.Component {
    render() {
        const linkProduct = '/product/' + this.props.product._id;
        const {title} = this.props.product;
        // const title = this.props.product.title.substring(0, 50) + (this.props.product.title.length > 50 ? '...' : '');

        return (
            <Card className='ProductGridCard'>
                <CardBody>
                    <Link className='ImageContainer' to={linkProduct}>
                        <img className='ImageContent' src={this.props.product.thumbnail} alt='thumbnail'/>
                    </Link>
                    <Link className='CardTitle' to={linkProduct}>{title}</Link>
                    <div className='Price'>
                        <span>${this.props.product.price.toFixed(2)}</span>
                    </div>
                    <Link to={linkProduct}>
                        <Button className='rounded-round' color='primary' size='sm' outline block>Order Now</Button>
                    </Link>
                </CardBody>
            </Card>
        );
    }
}

export default ProductGridCard;