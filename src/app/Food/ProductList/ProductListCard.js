import React from 'react';
import {Card, CardBody, Row} from "reactstrap";
import {Link} from "react-router-dom";

class ProductListCard extends React.Component {
    render() {
        const linkProduct = '/product/5c8995868186371010fecee9';

        return (
            <Card className='ProductListCard'>
                <CardBody>
                    <Row>
                        <div className='ImageView'>
                            <Link className='ImageContainer' to={linkProduct}>
                                <img className='ImageContent'
                                     src={'https://images.foody.vn/res/g5/43278/s570x570/20171027114326-goc-dui.jpg'}
                                     alt='thumbnail'/>
                            </Link>
                        </div>
                        <div className='MediaBody'>
                            <Link className='CardTitle' to={linkProduct}>Product Title</Link>
                            <div className='Price'>
                                <span>Product Price</span>
                            </div>
                        </div>
                    </Row>
                </CardBody>
            </Card>
        );
    }
}

export default ProductListCard;