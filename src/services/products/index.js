import axios from 'axios'
import configServer from '../../config/server';

export const reads = ({page, limit}) => {
    return axios.get(configServer.url + '/products', {params: {page, limit}});
}

export const read = (id) => {
    return axios.get(configServer.url + '/products/' + id)
}

export default {reads, read};