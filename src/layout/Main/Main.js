import React, {Component} from 'react';
import {Route} from 'react-router-dom'
import {Container} from "reactstrap";
import HomePage from "../../app/Home/HomePage";
import ProductPage from "../../app/Food/ProductPage/ProductPage";
import CheckoutPage from "../../app/Orders/CheckoutPage/CheckoutPage";
import InvoicePage from "../../app/Orders/InvoicePage/InvoicePage";
import ProductsPage from "../../app/Food/ProductsPage/ProductsPage";
import TestAll from "../../app/Test/TestAll";

class Main extends Component {
    render() {
        return (
            <div className='Main'>
                <Container className='Container'>
                    <Route exact path="/" component={HomePage}/>
                    <Route path='/store' component={ProductsPage}/>
                    <Route path='/product/:productId' component={ProductPage}/>
                    <Route path='/checkout/:productId' component={CheckoutPage}/>
                    <Route path='/invoice/:orderId' component={InvoicePage}/>
                    <Route path='/test/:id' component={TestAll}/>
                </Container>
            </div>
        );
    }
}

export default Main;