import React from 'react';
import {Col, Container, Row} from "reactstrap";
import logo from '../../assets/logo.png';
import {Link} from "react-router-dom";

class Footer extends React.Component {
    render() {
        return (
            <div className='Footer'>
                <Container>
                    <Row>
                        <Col sm={4} className='Info'>
                            <p className='Title'>Info</p>
                            <img src={logo} alt='logo'/>
                            <p>Order Food</p>
                            <p>Email: contact@orderfood.com</p>
                            <p>Phone: +17847982723</p>
                        </Col>
                        <Col sm={4} className='Service'>
                            <p className='Title'>Service</p>
                            <Link to={'/faq'}>FAQ</Link>
                            <Link to={'/terms-and-privacy'}>Terms & Privacy</Link>
                            <Link to={'/dcma'}>DCMA</Link>
                        </Col>
                        <Col sm={4} className='Company'>
                            <p className='Title'>Company</p>
                            <Link to={'/blog'}>Blog</Link>
                            <Link to={'/contact'}>Contact Us</Link>
                        </Col>
                    </Row>
                    <Row className='Copyright'>
                        <div>© Copyright 2019. Order Food.</div>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default Footer;